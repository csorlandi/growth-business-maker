export function createRequest(name, description) {
  return {
    type: '@project/CREATE_REQUEST',
    payload: { name, description },
  };
}

export function createSuccess() {
  return {
    type: '@project/CREATE_SUCCESS',
  };
}

export function createFailure() {
  return {
    type: '@project/CREATE_FAILURE',
  };
}

export function getAllRequest() {
  return {
    type: '@project/GET_ALL_REQUEST',
  };
}

export function getAllSuccess(projects) {
  return {
    type: '@project/GET_ALL_SUCCESS',
    payload: { projects },
  };
}

export function getAllFailure(error) {
  return {
    type: '@project/GET_ALL_FAILURE',
    payload: { error },
  };
}

export function deleteRequest(id) {
  return {
    type: '@project/DELETE_REQUEST',
    payload: { id },
  };
}

export function deleteFailure() {
  return {
    type: '@project/DELETE_FAILURE',
  };
}
