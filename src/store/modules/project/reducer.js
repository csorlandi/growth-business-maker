import produce from 'immer';

const INITIAL_STATE = {
  loading: true,
  projects: [],
  error: null,
};

export default function project(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case '@project/CREATE_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@project/CREATE_SUCCESS': {
        draft.loading = false;
        break;
      }
      case '@project/CREATE_FAILURE': {
        draft.loading = false;
        break;
      }
      case '@project/GET_ALL_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@project/GET_ALL_SUCCESS': {
        draft.loading = false;
        draft.projects = action.payload.projects;
        break;
      }
      case '@project/GET_ALL_FAILURE': {
        draft.loading = false;
        draft.error = action.payload.error;
        break;
      }
      case '@project/DELETE_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@project/DELETE_SUCCESS': {
        draft.loading = false;
        break;
      }
      case '@project/DELETE_FAILURE': {
        draft.loading = false;
        break;
      }
      default:
    }
  });
}
