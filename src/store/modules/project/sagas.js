import { takeLatest, all, put, select } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import firebase from 'firebase/app';

import {
  createSuccess,
  createFailure,
  getAllSuccess,
  getAllFailure,
  deleteFailure,
} from './actions';

import history from '~/services/history';
import slugify from '~/services/slugify';

export function* createProject({ payload }) {
  try {
    const authenticatedUser = yield select(store => store.firebase.auth);

    const { name, description } = payload;

    const nameSlug = slugify(name);

    yield firebase.ref(`projects/${nameSlug}`).set({
      name,
      description,
      members: {
        [authenticatedUser.uid]: true,
      },
    });

    yield firebase.ref(`users/${authenticatedUser.uid}/projects`).update({
      [nameSlug]: true,
    });

    yield put(createSuccess());

    history.push(`/dashboard/projeto/${nameSlug}`);
  } catch (err) {
    console.tron.log('try/catch error', err);
    toast.error('Falha ao Criar o Projeto, tente novamente!');
    yield put(createFailure());
  }
}

export function* getProjects() {
  try {
    const authenticatedUser = yield select(store => store.firebase.auth);

    let projects = yield firebase
      .ref(`users/${authenticatedUser.uid}/projects`)
      .once('value');

    projects = projects.val();

    const projectsArray = projects ? Object.keys(projects) : [];

    const userProjects = yield all(
      projectsArray.map(function*(item, index) {
        const project = yield firebase.ref(`projects/${item}`).once('value');

        return { ...project.val(), id: projectsArray[index] };
      })
    );

    yield put(getAllSuccess(userProjects));
  } catch (err) {
    console.tron.log('try/catch error do getAll', err.message);
    toast.error('Falha ao Listas os Projetos, tente novamente!');
    yield put(getAllFailure());
  }
}

export function* deleteProject({ payload }) {
  try {
    const { id } = payload;

    yield firebase.ref(`projects/${id}`).remove();

    const authenticatedUser = yield select(store => store.firebase.auth);

    yield firebase
      .ref(`users/${authenticatedUser.uid}/projects/${id}`)
      .remove();

    history.push('/dashboard/projetos/listar');
  } catch (err) {
    console.tron.log('try/catch error', err.message);
    toast.error('Falha ao Exluir o Projeto, tente novamente!');
    yield put(deleteFailure());
  }
}

export default all([
  takeLatest('@project/CREATE_REQUEST', createProject),
  takeLatest('@project/GET_ALL_REQUEST', getProjects),
  takeLatest('@project/DELETE_REQUEST', deleteProject),
]);
