import { takeLatest, all, put } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import firebase from 'firebase/app';

import { signFailure, signInSuccess } from './actions';

import history from '~/services/history';

export function* signIn() {
  try {
    yield firebase.login({ provider: 'google', type: 'popup' });

    yield put(signInSuccess());

    history.push('/dashboard');
  } catch (err) {
    console.tron.log('try/catch error', err.message);
    toast.error('Falha na Autenticação, verifique seus dados!');
    yield put(signFailure());
  }
}

export function* signOut() {
  yield firebase.logout();
  history.push('/');
}

export default all([
  takeLatest('@auth/SIGN_IN_REQUEST', signIn),
  takeLatest('@auth/SIGN_OUT', signOut),
]);
