import { combineReducers } from 'redux';

import { firebaseReducer } from 'react-redux-firebase';

import auth from './auth/reducer';
import project from './project/reducer';

export default combineReducers({
  firebase: firebaseReducer,
  auth,
  project,
});
