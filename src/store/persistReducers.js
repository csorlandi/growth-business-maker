import { persistReducer } from 'redux-persist';

import ReduxPersistConfig from '~/config/ReduxPersistConfig';

export default reducers => {
  const persistedReducer = persistReducer(ReduxPersistConfig, reducers);

  return persistedReducer;
};
