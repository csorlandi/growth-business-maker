import React from 'react';

import { useDispatch, useSelector } from 'react-redux';

import { signOut } from '~/store/modules/auth/actions';

import Notifications from '~/components/Notifications';

import { Container, Content, Profile } from './styles';

export default function DashboardHeader() {
  const dispatch = useDispatch();
  const user = useSelector(state => state.firebase.profile);

  function handleSignOut() {
    dispatch(signOut());
  }

  return (
    <Container>
      {/* <FaBars />
      <BreadcumbContainer>
        <FaHome />
        <FaChevronRight />
        <span>Usuários</span>
        <FaChevronRight />
        <span>Lista de Usuários</span>
      </BreadcumbContainer> */}
      <Content>
        <Notifications />
        <Profile>
          <div>
            <strong>{user.displayName}</strong>
            <button type="button" onClick={handleSignOut}>
              Sair
            </button>
          </div>
          <img src={user.avatarUrl} alt="Claudio Orlandi" />
        </Profile>
      </Content>
    </Container>
  );
}
