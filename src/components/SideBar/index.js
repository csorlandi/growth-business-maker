import React from 'react';

import { Link } from 'react-router-dom';

import { FaHome, FaUserCircle, FaUsers, FaLayerGroup } from 'react-icons/fa';

import SideBarMenu from './components/SideBarMenu';

import { Container, LogoContainer, ListContainer } from './styles';

import logo from '~/assets/logo_horizontal.svg';

export default function LeftBar() {
  return (
    <Container>
      <LogoContainer>
        <Link to="/dashboard">
          <img src={logo} alt="Logo" />
        </Link>
      </LogoContainer>
      <ListContainer>
        <SideBarMenu>
          <SideBarMenu.Item icon={<FaHome />} title="Home" to="/dashboard" />
          <SideBarMenu.Item
            icon={<FaUserCircle />}
            title="Meu Perfil"
            to="/dashboard/usuarios/perfil"
          />
          <SideBarMenu.Item icon={<FaUsers />} title="Times">
            <SideBarMenu.Item title="Meus Times" to="/dashboard/times/listar" />
            <SideBarMenu.Item title="Novo Time" to="/dashboard/times/criar" />
          </SideBarMenu.Item>
          <SideBarMenu.Item icon={<FaLayerGroup />} title="Projetos">
            <SideBarMenu.Item
              title="Meus Projetos"
              to="/dashboard/projetos/listar"
            />
            <SideBarMenu.Item
              title="Novo Projeto"
              to="/dashboard/projetos/criar"
            />
          </SideBarMenu.Item>
        </SideBarMenu>
      </ListContainer>
    </Container>
  );
}
