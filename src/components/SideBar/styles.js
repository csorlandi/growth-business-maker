import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  height: 100%;
  flex: 1;
  max-width: 22%;
  background-color: #faf8fb;
  border-left: 1px solid rgba(0, 72, 132, 0.15);
  flex-direction: column;
`;

export const LogoContainer = styled.div`
  height: 64px;
  display: flex;
  box-shadow: 0 0px 4px 0 rgba(0, 101, 184, 0.2);
  justify-content: center;
  align-items: center;

  a img {
    height: 48px;
  }
`;

export const ListContainer = styled.nav`
  display: flex;
  flex: 1;
`;
