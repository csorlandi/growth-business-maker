import React from 'react';
import PropTypes from 'prop-types';

import { FaChevronLeft, FaChevronDown } from 'react-icons/fa';

import SideBarMenuItemLink from './SideBarMenuItemLink';
import { SideBarItemContainer } from './styles';

export default function SideBarMenuItem({
  children,
  icon,
  title,
  to,
  subMenu: isSubMenu,
  item,
  updateItem,
}) {
  return (
    <SideBarItemContainer
      isSubMenu={isSubMenu}
      open={item.open}
      isActive={item.active}
    >
      <SideBarMenuItemLink
        to={to}
        onToggle={() => updateItem(item.id, { open: !item.open })}
      >
        {icon && <icon.type {...icon.props} />}
        <span>{title}</span>
        {children && (item.open ? <FaChevronDown /> : <FaChevronLeft />)}
      </SideBarMenuItemLink>
      {children && item.open && (
        <ul>
          {children.map((child, index) => (
            <child.type
              {...child.props}
              subMenu
              key={item.children[index].id}
              item={item.children[index]}
            />
          ))}
        </ul>
      )}
    </SideBarItemContainer>
  );
}

SideBarMenuItem.propTypes = {
  children: PropTypes.node,
  icon: PropTypes.node,
  title: PropTypes.string.isRequired,
  to: PropTypes.string,
  subMenu: PropTypes.bool,
  item: PropTypes.shape({
    id: PropTypes.string,
    open: PropTypes.bool,
    active: PropTypes.bool,
    children: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        open: PropTypes.bool,
        active: PropTypes.bool,
      })
    ),
  }),
  updateItem: PropTypes.func,
};

SideBarMenuItem.defaultProps = {
  children: null,
  icon: null,
  to: null,
  subMenu: false,
  item: {},
  updateItem: () => {},
};
