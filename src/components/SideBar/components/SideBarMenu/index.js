import SideBarMenu from './SideBarMenu';
import SideBarMenuItem from './SideBarMenuItem';

SideBarMenu.Item = SideBarMenuItem;

export default SideBarMenu;
