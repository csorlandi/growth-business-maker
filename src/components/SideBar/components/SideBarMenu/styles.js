import styled from 'styled-components';

export const SideBarContainer = styled.ul`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 32px 0;
`;

export const SideBarItemContainer = styled.li`
  padding: ${({ isSubMenu, open, isActive }) => {
    if (!isActive) {
      if (isSubMenu) return '8px 24px';

      if (open) return '8px 16px 0 16px';

      return '8px 16px';
    }
    if (isSubMenu) return '8px 24px 8px 20px';

    if (open) return '8px 16px 0 12px';

    return '8px 16px 8px 12px';
  }};
  border-left: ${({ isActive }) => isActive && '4px solid #004884'};
  background: ${({ isActive }) =>
    isActive ? 'rgba(0, 72, 132, 0.025)' : 'transparent'};

  button {
    padding-bottom: ${({ open }) => (open ? '8px' : '0')};
  }

  a,
  button {
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 100%;
    border: 0;
    background: transparent;
    color: ${({ isActive }) => (!isActive ? '#8493a5' : '#004884')};

    &:active,
    &:visited,
    &:active {
      color: ${({ isActive }) => (!isActive ? '#8493a5' : '#004884')};
    }

    svg {
      margin-right: 8px;
      font-size: 16px;

      &:last-child {
        margin-right: 0;
        font-size: 12px;
      }
    }

    span {
      display: flex;
      flex: 1;
      font-size: ${({ isSubMenu }) => (isSubMenu ? '14px' : '16px')};
    }
  }
`;
