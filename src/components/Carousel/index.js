import React from 'react';
import { Gallery, GalleryImage } from 'react-gesture-gallery';

import { Container } from './styles';

import img1 from '~/assets/Carousel/img1.png';
import img2 from '~/assets/Carousel/img2.png';
import img4 from '~/assets/Carousel/img4.png';
import img5 from '~/assets/Carousel/img5.png';
import img7 from '~/assets/Carousel/img7.png';

const images = [img1, img2, img4, img5, img7];

export default function Carousel() {
  const [index, setIndex] = React.useState(0);

  React.useEffect(() => {
    const timer = setInterval(() => {
      if (index === 4) {
        setIndex(0);
      } else {
        setIndex(prev => prev + 1);
      }
    }, 3000);
    return () => clearInterval(timer);
  }, [index]);

  return (
    <Container>
      <Gallery
        style={{
          background: 'transparent',
          height: '500px',
          width: '800px',
        }}
        index={index}
        onRequestChange={i => {
          setIndex(i);
        }}
      >
        {images.map(image => (
          <GalleryImage objectFit="contain" key={image} src={image} />
        ))}
      </Gallery>
    </Container>
  );
}
