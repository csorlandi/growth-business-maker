import styled from 'styled-components';

export const Container = styled.div`
  /* background: rgb(0, 73, 132); */
  /* background: #f2f9ff; */
  background: rgba(0, 73, 132, 0.7);
  padding: 0 16px;
  display: flex;
  height: 500px;
  width: 800px;
  justify-content: center;
  flex-direction: row;
  align-items: center;
  position: sticky;
  z-index: 1;
  box-shadow: 0px 0px 18px 1px rgba(0, 0, 0, 0.7);
  /* min-height: 300px; */
  max-height: 800px;
`;
