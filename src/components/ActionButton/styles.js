import styled from 'styled-components';
import TextArea from 'react-textarea-autosize';

export const AddButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 8px 16px;
  background-color: #004884;
  border-radius: 8px;
  align-self: baseline;
  color: #fff;

  svg {
    margin-right: 8px;
  }
`;

export const CardContainer = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 4px;
  padding: 16px;
  background-color: #eee;
`;

export const Input = styled(TextArea).attrs({
  minRows: 3,
  placeholder: '...',
  autoFocus: true,
})`
  width: 100%;
  resize: none;
  font-size: 16px;
  padding: 4px;
  border-radius: 4px;
  border: 0;
`;

export const ButtonsContainer = styled.div`
  margin-top: 16px;
  display: flex;
  align-items: center;
`;

export const SaveButton = styled.button`
  display: flex;
  border: 0;
  color: #fff;
  background-color: #5aac44;
  font-size: 18px;
  font-weight: bold;
  padding: 4px 8px;
  border-radius: 4px;
  align-items: center;

  svg {
    margin-right: 8px;
  }
`;

export const CloseButton = styled.button`
  border: 0;
  color: #999;
  background-color: transparent;
  font-size: 18px;
  margin-left: 16px;
`;
