import React, { useState } from 'react';

import { FaPlus, FaTimes, FaSave } from 'react-icons/fa';

import {
  AddButtonContainer,
  CardContainer,
  Input,
  ButtonsContainer,
  SaveButton,
  CloseButton,
} from './styles';

export default function ActionButton({ addCard }) {
  const [formOpen, setFormOpen] = useState(false);
  const [input, setInput] = useState('');

  function handleSave() {
    if (!input) return;

    addCard(input);

    setInput('');
    setFormOpen(false);
  }

  function renderForm() {
    return (
      <CardContainer>
        <Input value={input} onChange={e => setInput(e.target.value)} />
        <ButtonsContainer>
          <SaveButton onClick={handleSave}>
            <FaSave />
            <span>Salvar</span>
          </SaveButton>
          <CloseButton onClick={() => setFormOpen(false)}>
            <FaTimes />
          </CloseButton>
        </ButtonsContainer>
      </CardContainer>
    );
  }

  function renderAddButton() {
    return (
      <AddButtonContainer onClick={() => setFormOpen(true)}>
        <FaPlus />
        <p>Adicionar</p>
      </AddButtonContainer>
    );
  }

  return formOpen ? renderForm() : renderAddButton();
}
