import React from 'react';
import PropTypes from 'prop-types';

import { TitleContainer } from './styles';

export default function TitleBar({ title, button, buttonCategory }) {
  return (
    <TitleContainer buttonCategory={buttonCategory}>
      <h1>{title}</h1>
      {button && <button.type {...button.props} />}
    </TitleContainer>
  );
}

TitleBar.propTypes = {
  title: PropTypes.string.isRequired,
  button: PropTypes.node,
  buttonCategory: PropTypes.string,
};

TitleBar.defaultProps = {
  button: null,
  buttonCategory: 'success',
};
