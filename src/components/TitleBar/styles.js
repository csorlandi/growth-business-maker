import styled from 'styled-components';

export const TitleContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 8px;
  border-bottom: 1px solid #eee;

  h1 {
    font-size: 32px;
  }

  a,
  button {
    align-self: baseline;
    border: 0;
    background-color: ${({ buttonCategory }) => {
      switch (buttonCategory) {
        case 'success':
          return '#25a18e';
        case 'danger':
          return '#fb3640';
        default:
          return '#25a18e';
      }
    }};
    padding: 8px;
    border-radius: 4px;
    font-size: 14px;
    color: #fff;
    font-weight: bold;
    display: flex;
    align-items: center;

    svg {
      font-size: 14px;
      margin-right: 8px;
    }
  }
`;
