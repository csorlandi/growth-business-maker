import React from 'react';
import { Draggable } from 'react-beautiful-dnd';

import { Container, Text } from './styles';

export default function ContentCard({ text, id, index }) {
  return <Draggable draggableId={String(id)} index={index}>
    {provided => (
      <Container ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
        <Text>{text}</Text>
      </Container>
    )}
  </Draggable>;
}
