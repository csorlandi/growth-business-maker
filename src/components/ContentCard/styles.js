import styled from 'styled-components';

export const Container = styled.div`
  border-radius: 4px;
  display: flex;
  padding: 8px;
  margin-bottom: 8px;
  background-color: #eee;
  align-self: stretch;
`;

export const Text = styled.p`
  display: flex;
  flex: 1;
`;
