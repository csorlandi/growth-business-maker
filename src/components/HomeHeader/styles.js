import styled from 'styled-components';

export const Container = styled.div`
  background: #f2f9ff;
  padding: 0 16px;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  position: sticky;
  box-shadow: 0px 0px 8px 1px rgba(0, 0, 0, 0.2);
  z-index: 2;
  height: 70px;
`;

export const Content = styled.aside`
  flex: 1;
  height: 64px;
  max-width: 1366px;
  margin: 0 auto;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  nav {
    display: flex;
    align-items: center;
    img {
      margin-right: 20px;
      padding-right: 20px;
    }
    a {
      font-weight: bold;
      color: #ebf6ff;
    }
  }
  aside {
    display: flex;
    align-items: center;
  }
`;

export const Profile = styled.div`
  display: flex;
  margin-left: 20px;
  padding-left: 20px;
  border-left: 1px solid rgba(0, 73, 132);
  div {
    text-align: right;
    margin-right: 10px;
    strong {
      display: block;
      color: #f2f9ff;
    }
  }
  img {
    width: 32px;
    height: 32px;
    border-radius: 50%;
  }
`;

export const NavBtn = styled.div`
  height: 20px;
  padding: 20px 10px;
  border-radius: none;
  border: none;
  color: rgb(0, 73, 132);
  background-color: transparent;
  font-weight: bold;
  font-size: 18px;
  display: flex;
  align-items: center;
  background-image: linear-gradient(rgb(0, 73, 132), rgb(0, 73, 132));
  background-size: 0 5px, auto;
  background-repeat: no-repeat;
  background-position: center bottom;
  transition: all 0.3s ease-out;
  &:hover {
    background-size: 100% 5px, auto;
  }
`;

export const SignInBtn = styled.div`
  color: #f2f9ff;
  padding: 10px 20px;
  border-radius: 20px;
  font-size: 12px;
  border: 2px solid rgb(0, 73, 132);
  font-family: inherit;
  background-color: rgb(0, 73, 132);
  font-weight: bold;
  &:hover {
    color: rgb(0, 73, 132);
    background-color: transparent;
    border-color: rgb(0, 73, 132);
  }
`;
