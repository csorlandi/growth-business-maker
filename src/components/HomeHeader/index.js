import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { Container, Content, Profile, NavBtn, SignInBtn } from './styles';

export default function HomeHeader() {
  const isAuthenticated = useSelector(state => state.auth.signed);

  function renderAuthButton() {
    if (isAuthenticated) {
      return (
        <Link to="/dashboard">
          <SignInBtn type="button" onClick>
            Dashboard
          </SignInBtn>
        </Link>
      );
    }
    return (
      <Link to="/login">
        <SignInBtn type="button" onClick>
          Login
        </SignInBtn>
      </Link>
    );
  }

  return (
    <Container>
      <Content>
        <NavBtn type="button">Home</NavBtn>
        <NavBtn type="button">GBM</NavBtn>
        <NavBtn type="button">Contato</NavBtn>
        <Profile>
          <div>{renderAuthButton()}</div>
        </Profile>
      </Content>
    </Container>
  );
}
