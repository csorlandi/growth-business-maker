import React from 'react';
import { Switch } from 'react-router-dom';
import Route from './Route';

import Home from '~/pages/Home';

import SignIn from '~/pages/SignIn';

import Dashboard from '~/pages/Dashboard';
import MyProjects from '~/pages/MyProjects';
import NewProject from '~/pages/NewProject';
import ProjectDetails from '~/pages/ProjectDetails';
import CustomerProfile from '~/pages/CustomerProfile';
import ValueMap from '~/pages/ValueMap';
import BusinessModel from '~/pages/BusinessModel';
import SwotAnalysis from '~/pages/SwotAnalysis';

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} layout="home" />

      <Route path="/login" component={SignIn} />

      <Route path="/dashboard" exact component={Dashboard} isPrivate />
      <Route
        path="/dashboard/projetos/listar"
        exact
        component={MyProjects}
        isPrivate
      />
      <Route
        path="/dashboard/projetos/criar"
        component={NewProject}
        exact
        isPrivate
      />

      <Route
        path="/dashboard/projeto/:project_id"
        component={ProjectDetails}
        exact
        isPrivate
      />

      <Route
        path="/dashboard/projeto/:project_id/perfil-do-consumidor"
        component={CustomerProfile}
        exact
        isPrivate
      />
      <Route
        path="/dashboard/projeto/:project_id/mapa-de-valor"
        component={ValueMap}
        exact
        isPrivate
      />
      <Route
        path="/dashboard/projeto/:project_id/canvas-de-negocio"
        component={BusinessModel}
        exact
        isPrivate
      />
      <Route
        path="/dashboard/projeto/:project_id/analise-swot"
        component={SwotAnalysis}
        exact
        isPrivate
      />
    </Switch>
  );
}
