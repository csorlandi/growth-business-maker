import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect, withRouter } from 'react-router-dom';

import AuthLayout from '~/pages/_layouts/auth';
import DefaultLayout from '~/pages/_layouts/default';
import HomeLayout from '~/pages/_layouts/home';

import { store } from '~/store';

function RouteWrapper({
  component: Component,
  isPrivate,
  layout,
  location,
  ...rest
}) {
  const { signed } = store.getState().auth;

  if (!signed && isPrivate) {
    return <Redirect to="/" />;
  }

  if (signed && !isPrivate && location.pathname !== '/') {
    return <Redirect to="/dashboard" />;
  }

  let Layout = null;

  if (layout) {
    switch (layout) {
      case 'home':
        Layout = HomeLayout;
        break;
      case 'auth':
        Layout = AuthLayout;
        break;
      case 'default':
        Layout = DefaultLayout;
        break;
      default:
        break;
    }
  } else {
    Layout = signed ? DefaultLayout : AuthLayout;
  }

  return (
    <Route
      {...rest}
      render={props => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  );
}

RouteWrapper.propTypes = {
  isPrivate: PropTypes.bool,
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func])
    .isRequired,
  layout: PropTypes.string,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
};

RouteWrapper.defaultProps = {
  isPrivate: false,
  layout: '',
};

export default withRouter(RouteWrapper);
