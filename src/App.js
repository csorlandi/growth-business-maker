import React from 'react';
import { ToastContainer } from 'react-toastify';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';

import { ReactReduxFirebaseProvider } from 'react-redux-firebase';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

import './config/ReactotronConfig';
import FirebaseConfig from './config/FirebaseConfig';

import Routes from './routes';
import history from './services/history';

import { store, persistor } from './store';

import GlobalStyle from './styles/global';

firebase.initializeApp(FirebaseConfig);

const rrfProps = {
  firebase,
  config: {
    userProfile: 'users',
    enableLogging: 'true',
  },
  dispatch: store.dispatch,
};

function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <ReactReduxFirebaseProvider {...rrfProps}>
          <Router history={history}>
            <Routes />
            <GlobalStyle />
            <ToastContainer autoClose={3000} />
          </Router>
        </ReactReduxFirebaseProvider>
      </PersistGate>
    </Provider>
  );
}

export default App;
