import storage from 'redux-persist/lib/storage';

const ReduxPersistConfig = {
  key: 'gbm',
  storage,
  whitelist: ['auth', 'user'],
};

export default ReduxPersistConfig;
