import React from 'react';

import { useDispatch, useSelector } from 'react-redux';

import { signInRequest } from '~/store/modules/auth/actions';

import logo from '~/assets/logo.svg';

export default function SignIn() {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.auth.loading);

  function handleSubmit() {
    dispatch(signInRequest());
  }

  return (
    <>
      <img src={logo} alt="Pet São João" />
      <form>
        <button type="button" onClick={() => handleSubmit()}>
          {loading ? 'Carregando...' : 'Entrar com Google'}
        </button>
      </form>
    </>
  );
}
