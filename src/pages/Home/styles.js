import styled from 'styled-components';

import bg from '~/assets/img/55555.jpg';
import bg2 from '~/assets/img/background-3045402_1920.png';

export const TopContainer = styled.div`
  /* background: rgb(0, 73, 132); */
  background: #f2f9ff;
  /* background: rgba(0, 73, 132, 0.7); */
  padding: 0 16px;
  display: flex;
  height: (100vh - 70px);
  justify-content: center;
  flex-direction: row;
  align-items: center;
  position: sticky;
  z-index: 1;
  box-shadow: 0px 0px 18px 1px rgba(0, 0, 0, 0.7);
  /* min-height: 300px; */
  max-height: 800px;
`;

export const Container = styled.div`
  /* background: rgb(0, 73, 132); */
  background: #f2f9ff;
  padding: 0 16px;
  display: flex;
  height: 50vh;
  position: sticky;
  z-index: 1;
  justify-content: space-evenly;
  flex-direction: row;
  align-items: center;
  box-shadow: 0px 0px 18px 1px rgba(0, 0, 0, 0.7);
  position: fixed top;
`;

export const Card = styled.article`
  color: #f2f9ff;
  height: 300px;
  width: 300px;
  padding: 25px;
  text-align: center;
  background: rgba(0, 73, 132, 0.7);
  box-shadow: 0px 0px 18px 1px rgba(0, 0, 0, 0.7);
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;

export const Aside = styled.aside`
  height: 360px;
  width: 599px;
  background: rgba(0, 73, 132, 0.7);
  box-shadow: 0px 0px 18px 1px rgba(0, 0, 0, 0.7);
  display: flex;
  justify-content: center;
  flex-direction: row;
  align-items: center;
`;

export const Parallax1 = styled.div`
  height: 100vh;
  min-height: 400px;
  max-height: 911px;
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  background-color: #f2f9ff;
  background-image: url(${bg});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  background-attachment: fixed;
  div {
    padding: 100px 50px;
    /* border: 5px solid rgba(125, 125, 125, 0.8);

    background: rgba(255, 255, 255, 0.5); */
  }
  h1 {
    color: #000;
    font-size: 70px;
    margin: 10px;
  }
  h3 {
    color: #888;
    font-size: 30px;
    margin: 10px;
    font-style: oblique;
  }
  p {
    color: #333;
    /* text-shadow: black 0.1em 0.1em 0.2em; */
    margin: 2px;
    font-size: 25px;
    max-width: 1000px;
    text-align: center;
  }
`;

export const Parallax2 = styled.div`
  height: 40vh;
  min-height: 200px;
  max-height: 400px;
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  background-color: #f2f9ff;
  background-image: url(${bg2});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  background-attachment: fixed;
  div {
    padding: 100px 50px;
    /* border: 5px solid rgba(125, 125, 125, 0.8);

    background: rgba(255, 255, 255, 0.5); */
  }
  h1 {
    color: #000;
    font-size: 70px;
    margin: 10px;
    text-shadow: black 0.1em 0.1em 0.2em;
  }
  h3 {
    color: #000;
    font-size: 30px;
    margin: 10px;
    font-style: oblique;
    text-shadow: black 0.1em 0.1em 0.2em;
  }
  p {
    color: #333;
    text-shadow: black 0.1em 0.1em 0.2em;
    margin: 5px;
    font-size: 25px;
    max-width: 1000px;
    text-align: center;
  }
`;

export const Footer = styled.footer`
  height: 60px;
  background: #f2f9ff;
  display: flex;
  justify-content: center;
  flex-direction: row;
  align-items: center;
  position: sticky;
  z-index: 3;
`;
