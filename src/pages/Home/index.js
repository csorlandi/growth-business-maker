import React from 'react';

import {
  TopContainer,
  Container,
  Parallax1,
  Parallax2,
  Card,
  Footer,
  Aside,
} from './styles';

import Carousel from '~/components/Carousel';
import team from '~/assets/img/team.png';

export default function Home() {
  return (
    <>
      <TopContainer>
        <Carousel />
      </TopContainer>
      <Parallax1>
        <div>
          <h1>Growth Business Maker</h1>
          <h3>
            &quot;A melhor maneira de validar a assertividade de seu
            empreendimento&quot;
          </h3>
          <p>
            Com o objetivo de auxiliar novos empreendedores, estudantes, ou até
            mesmo curiosos, a Growth Business Maker, ajuda a planejar e
            monitorar o modelo de seu novo negócio.
          </p>
          <p>
            O sistema permite uma visão ampla e horizontal de todo o progresso
            realizado no modelo, feito por você, ou seu time.
          </p>
          <p>
            Apresenta dicas e truques de como fazer um bom modelo de negócio
            definindo metas, verificando os passos realizados e acompanhando o
            seu progesso exibindo dados dinâmicos contendo índices quantitativos
            e qualitativos.
          </p>
          <p>
            Além de proporcionar maior controle, produtividade e otimização dos
            recursos disponibilizados, o sistema aliado as boas práticas, visa
            mitigar os riscos de iniciar um novo empreendimento.
          </p>
        </div>
      </Parallax1>
      <Container>
        <Card>
          <h2>Nossa Missão:</h2>
          <h3>
            Entregar um sistema de qualidade, que ajude a prover acesso a
            informação e a ferramenta de modelagem de negócio.
          </h3>
        </Card>
        <Card>
          <h2>Os benefícios :</h2>
          <h3>Gratuito, para sempre.</h3>
          <h3>Acessível, suportado em qualquer Navegador.</h3>
          <h3>Suporta trabalho em equipe!</h3>
          <h3>Dinâmico, voltado para o Usuário!</h3>
        </Card>
        <Card>
          <h2>Próximos Passos:</h2>
          <h3>Integração com o Classroom!</h3>
          <h3>Parcerias!</h3>
        </Card>
      </Container>
      <Parallax2>
        <h1>De nós, para o Mundo!</h1>
        <h3>Apoiamos a Iniciativa Open Source!</h3>
      </Parallax2>
      <Container>
        <Aside>
          <img src={team} alt="Nosso time" />
        </Aside>
        <Card>
          <h1>Nosso Time:</h1>
          <h2>· Claudio ·</h2>
          <h2>· Murillo ·</h2>
          <h2>· Otto ·</h2>
          <h1>________</h1>
          <h1>Contato:</h1>
          <h3>contato.gbm@growthlabs.com</h3>
        </Card>
      </Container>
      <Footer>
        <h3>Copyrigth © 2019 GrowthLABS - Growth Business Maker</h3>
      </Footer>
    </>
  );
}
