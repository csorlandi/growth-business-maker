import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 8px;
  border-radius: 8px;
  background-color: #fff;
`;

export const ContentContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: space-evenly;
  margin-top: 16px;

  span {
    margin-top: 16px;
    font-size: 14px;
    font-weight: 600;

    &.loading {
      color: #aaa;
    }

    &.empty {
      color: #a40606;
    }
  }
`;

export const CardContainer = styled.div`
  flex-direction: column;
  width: 21%;
  box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.15), 0 6px 20px 0 rgba(0, 0, 0, 0.15);
  padding: 16px 16px 0 16px;
  border-radius: 8px;
  margin-bottom: 16px;

  h2 {
    font-size: 18px;
    margin-bottom: 8px;
  }

  hr {
    border-width: 0.5px;
    color: #666;
  }

  a {
    text-decoration: none;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    padding: 8px 0;
    margin: 8px 0;
    color: #004884;
    font-weight: bold;

    svg {
      margin-left: 4px;
    }

    &:hover,
    &:active,
    &:visited {
      font-weight: bold;
      color: #004884;
    }

    span {
      margin: 0;
    }
  }
`;
