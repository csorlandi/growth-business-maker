import React, { useEffect } from 'react';

import { Link } from 'react-router-dom';

import { FaPlus, FaChevronRight } from 'react-icons/fa';

import { useDispatch, useSelector } from 'react-redux';
import { getAllRequest } from '~/store/modules/project/actions';

import TitleBar from '~/components/TitleBar';

import { Container, ContentContainer, CardContainer } from './styles';

export default function MyProjects() {
  const dispatch = useDispatch();
  const projects = useSelector(store => store.project.projects);
  const loading = useSelector(store => store.project.loading);
  const firebaseLoaded = useSelector(store => store.firebase.auth.isLoaded);

  useEffect(() => {
    if (firebaseLoaded) {
      dispatch(getAllRequest());
    }
  }, [firebaseLoaded]); //eslint-disable-line

  function renderTitleButton() {
    return (
      <Link to="/dashboard/projetos/criar">
        <FaPlus />
        <span>Novo Projeto</span>
      </Link>
    );
  }

  function renderCards() {
    return projects.map(item => {
      console.tron.log('item', item);
      return (
        <CardContainer>
          <h2>{item.name}</h2>
          <hr />
          <Link to={`/dashboard/projeto/${item.id}`}>
            <span>Acessar</span>
            <FaChevronRight />
          </Link>
        </CardContainer>
      );
    });
  }

  function renderEmptyMessage() {
    return <span className="empty">Você não possui nenhum projeto!</span>;
  }

  function renderLoading() {
    return (
      <span className="loading">Estamos carregando seus projetos ...</span>
    );
  }

  function renderContent() {
    if (loading) return renderLoading();

    if (!projects.length) return renderEmptyMessage();

    return renderCards();
  }

  return (
    <Container>
      <TitleBar title="Meus Projetos" button={renderTitleButton()} />
      <ContentContainer>{renderContent()}</ContentContainer>
    </Container>
  );
}
