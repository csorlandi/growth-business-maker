import React, { useState, useEffect } from 'react';
import firebase from 'firebase/app';

import { useParams, Link } from 'react-router-dom';

import { FaTrash } from 'react-icons/fa';

import { useDispatch } from 'react-redux';
import { deleteRequest } from '~/store/modules/project/actions';

import TitleBar from '~/components/TitleBar';

import { Container } from './styles';

export default function ProjectDetails() {
  const dispatch = useDispatch();
  const { project_id } = useParams();
  const [project, setProject] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function getProjectDetails() {
      let projectDetails = await firebase
        .ref(`projects/${project_id}`)
        .once('value');

      projectDetails = projectDetails.val();

      setProject(projectDetails);
      setLoading(false);
    }

    getProjectDetails();
  }, []); //eslint-disable-line

  function handleClick() {
    dispatch(deleteRequest(project_id));
  }

  function renderTitleButton() {
    return (
      <button type="button" onClick={handleClick}>
        <FaTrash />
        <span>Excluir Projeto</span>
      </button>
    );
  }

  return (
    <Container>
      {loading ? (
        <span className="loading">
          Estamos carregando os detalhes do seu projeto ...
        </span>
      ) : (
        <>
          <TitleBar
            title={project.name || ''}
            button={renderTitleButton()}
            buttonCategory="danger"
          />
          <p>{project.description}</p>
          <div className="items-container">
            <table>
              <thead>
                <th>Opções</th>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <Link
                      to={`/dashboard/projeto/${project_id}/perfil-do-consumidor`}
                    >
                      Perfil do Consumidor
                    </Link>
                  </td>
                </tr>
                <tr>
                  <td>
                    <Link to={`/dashboard/projeto/${project_id}/mapa-de-valor`}>
                      Mapa de Valor
                    </Link>
                  </td>
                </tr>
                <tr>
                  <td>
                    <Link
                      to={`/dashboard/projeto/${project_id}/canvas-de-negocio`}
                    >
                      Canvas de Negócio
                    </Link>
                  </td>
                </tr>
                <tr>
                  <td>
                    <Link to={`/dashboard/projeto/${project_id}/analise-swot`}>
                      Análise SWOT
                    </Link>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </>
      )}
    </Container>
  );
}
