import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 8px;
  border-radius: 8px;
  background-color: #fff;

  span {
    &.loading {
      text-align: center;
      font-size: 18px;
      margin-top: 32px;
      font-weight: bold;
      color: #666;
    }
  }

  p {
    font-size: 16px;
    color: #999;
    margin: 16px 8px;
  }

  div {
    &.items-container {
      display: flex;
      flex: 1;
      align-items: center;
      justify-content: center;

      table {
        width: 35%;
        border: 1px solid #ddd;
        border-radius: 8px;

        thead {
          th {
            font-size: 16px;
            font-weight: bold;
            color: #666;
            border-bottom: 1px solid #ddd;
            padding: 8px 0;
          }
        }

        tbody {
          tr {
            td {
              border-bottom: 1px solid #ddd;

              a {
                display: flex;
                padding: 16px;
                font-size: 14px;
                color: ${({ itemCompleted }) => {
                  console.tron.log('itemCompleted', itemCompleted);
                  return '#999';
                }};
                text-decoration: none;

                &:hover,
                &:active,
                &:visited {
                  color: #999;
                }
              }
            }

            &:hover {
              background-color: #004884;
              cursor: pointer;

              td a {
                color: #fff;
                font-weight: bold;
              }
            }

            &:last-child {
              td {
                border-bottom: 0;
              }
            }
          }
        }
      }
    }
  }
`;
