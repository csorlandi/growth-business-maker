import React from 'react';
import PropTypes from 'prop-types';

import DashboardHeader from '~/components/DashboardHeader';
import SideBar from '~/components/SideBar';

import {
  Container,
  Content,
  RightContent,
  MainContentContainer,
} from './styles';

export default function DefaultLayout({ children }) {
  return (
    <Container>
      <Content>
        <SideBar />
        <RightContent>
          <DashboardHeader />
          <MainContentContainer>{children}</MainContentContainer>
        </RightContent>
      </Content>
    </Container>
  );
}

DefaultLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
