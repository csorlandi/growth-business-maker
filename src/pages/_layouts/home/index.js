import React from 'react';
import PropTypes from 'prop-types';

import HomeHeader from '~/components/HomeHeader';

import { Container, Content, MainContentContainer } from './styles';

export default function DefaultLayout({ children }) {
  return (
    <Container>
      <HomeHeader />
      <Content>
        <MainContentContainer>{children}</MainContentContainer>
      </Content>
    </Container>
  );
}

DefaultLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
