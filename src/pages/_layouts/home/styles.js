import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  width: 100vw;
  height: 100%;
  flex: 1;
  flex-direction: column;
  background-color: #e2e3eb;
  /* background-color: #ebf6ff; */
  justify-content: center;
  align-items: center;
  font-family: inherit;
`;

export const Content = styled.div`
  display: flex;
  width: 100%;
  flex: 1;
  max-width: 1366px;
  /* border: 5px solid red; */
  overflow-y: scroll;
`;

export const MainContentContainer = styled.div`
  background-color: #e2e3eb;
  flex: 1;
  /* border: 5px solid green; */
  height: 1500px;
`;
