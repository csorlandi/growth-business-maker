import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 8px;
  border-radius: 8px;
  background-color: #fff;
`;

export const ContentContainer = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;

  form {
    width: 40%;
    padding: 16px;
    border-radius: 16px;
    box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.15),
      0 6px 20px 0 rgba(0, 0, 0, 0.15);
    display: flex;
    flex-direction: column;

    input {
      font-size: 14px;
      border: 1px solid rgba(0, 0, 0, 0.25);
      border-radius: 4px;
      height: 44px;
      padding: 0 15px;
      color: #000;
      margin: 0 0 10px;

      &::placeholder {
        color: rgba(0, 0, 0, 0.35);
      }
    }

    textarea {
      font-size: 14px;
      border: 1px solid rgba(0, 0, 0, 0.25);
      border-radius: 4px;
      padding: 15px;
      color: #000;
      margin: 0 0 10px;

      &::placeholder {
        color: rgba(0, 0, 0, 0.35);
      }
    }

    button {
      height: 44px;
      border: 0;
      padding: 0 15px;
      color: #fff;
      background-color: #25a18e;
      border-radius: 4px;
      font-size: 16px;
      font-weight: bold;
    }
  }
`;
