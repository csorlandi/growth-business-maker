import React, { useState } from 'react';

import { useDispatch } from 'react-redux';
import { createRequest } from '~/store/modules/project/actions';

import TitleBar from '~/components/TitleBar';

import { Container, ContentContainer } from './styles';

export default function NewProject() {
  const dispatch = useDispatch();

  const [nameInput, setNameInput] = useState('');
  const [descriptionInput, setDescriptionInput] = useState('');

  function handleSubmit() {
    if (!nameInput || !descriptionInput) return;

    dispatch(createRequest(nameInput, descriptionInput));
  }

  return (
    <Container>
      <TitleBar title="Criar Projeto" />
      <ContentContainer>
        <form>
          <input
            type="text"
            value={nameInput}
            onChange={e => setNameInput(e.target.value)}
            placeholder="Nome do Projeto"
          />
          <textarea
            rows="10"
            value={descriptionInput}
            onChange={e => setDescriptionInput(e.target.value)}
            placeholder="Breve Descrição"
          />
          <button type="button" onClick={handleSubmit}>
            Criar
          </button>
        </form>
      </ContentContainer>
    </Container>
  );
}
