import React from 'react';

import TitleBar from '~/components/TitleBar';

import { Container } from './styles';

export default function Dashboard() {
  return (
    <Container>
      <TitleBar title="Página Inicial" />
    </Container>
  );
}
