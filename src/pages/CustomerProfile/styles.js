import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 8px;
  border-radius: 8px;
  background-color: #fff;

  div {
    &.content {
      display: flex;
      flex-direction: column;
      flex: 1;
      border: 2px solid #999;
      border-radius: 8px;
      margin-top: 8px;
    }
  }
`;

export const CardsContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  h4 {
    margin-bottom: 16px;
  }
`;

export const FirstLine = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
`;

export const SecondLine = styled.div`
  display: flex;
  height: 33%;
  border-top: 2px solid #999;
  padding: 16px;
`;

export const FirstColumn = styled.div`
  display: flex;
  flex: 1;
  border-right: 2px solid #999;
  padding: 16px;
`;

export const SecondColumn = styled.div`
  display: flex;
  flex: 1;
  padding: 16px;
`;
