import React, { useState, useEffect } from 'react';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import { useParams } from 'react-router-dom';
import uuid from 'uuid/v4';

import firebase from 'firebase/app';

import TitleBar from '~/components/TitleBar';
import ContentCard from '~/components/ContentCard';
import ActionButton from '~/components/ActionButton';

import {
  Container,
  CardsContainer,
  FirstLine,
  FirstColumn,
  SecondColumn,
  SecondLine,
} from './styles';

export default function CustomerProfile() {
  const [gains, setGains] = useState({ loading: true, cards: [] });
  const [pains, setPains] = useState({ loading: true, cards: [] });
  const [jobs, setJobs] = useState({ loading: true, cards: [] });
  const [loading, setLoading] = useState(true);

  const { project_id } = useParams();

  useEffect(() => {
    async function setInitialState() {
      const data = await firebase
        .ref(`projects/${project_id}/items/customer-profile`)
        .once('value');

      setGains({
        ...gains,
        id: uuid(),
        loading: false,
        cards: data.val() ? data.val().gains : [],
      });
      setPains({
        ...pains,
        id: uuid(),
        loading: false,
        cards: data.val() ? data.val().pains : [],
      });
      setJobs({
        ...jobs,
        id: uuid(),
        loading: false,
        cards: data.val() ? data.val().jobs : [],
      });
      setLoading(false);
    }

    setInitialState();
  }, []); //eslint-disable-line

  useEffect(() => {
    async function persistGains() {
      await firebase
        .ref(`projects/${project_id}/items/customer-profile/gains`)
        .set(gains.cards);
    }

    if (!loading) {
      persistGains();
    }
  }, [gains]); //eslint-disable-line

  useEffect(() => {
    async function persistPains() {
      await firebase
        .ref(`projects/${project_id}/items/customer-profile/pains`)
        .set(pains.cards);
    }

    if (!loading) {
      persistPains();
    }
  }, [pains]); //eslint-disable-line

  useEffect(() => {
    async function persistJobs() {
      await firebase
        .ref(`projects/${project_id}/items/customer-profile/jobs`)
        .set(jobs.cards);
    }

    if (!loading) {
      persistJobs();
    }
  }, [gains]); //eslint-disable-line

  function addGains(text) {
    setGains({ ...gains, cards: [...gains.cards, { id: uuid(), text }] });
  }

  function addPains(text) {
    setPains({ ...pains, cards: [...pains.cards, { id: uuid(), text }] });
  }

  function addJobs(text) {
    setJobs({ ...jobs, cards: [...jobs.cards, { id: uuid(), text }] });
  }

  function getList(id) {
    if (id === gains.id) {
      return gains;
    }
    if (id === pains.id) {
      return pains;
    }
    return jobs;
  }

  function onDragEnd(result) {
    const { destination, source } = result;

    if (!destination) return;

    if (source.droppableId === destination.droppableId) {
      const list = getList(destination.droppableId);
      const card = list.cards.splice(source.index, 1);
      list.cards.splice(destination.index, 0, ...card);
    }

    if (source.droppableId !== destination.droppableId) {
      const listStart = getList(source.droppableId);

      const card = listStart.cards.splice(source.index, 1);

      const listEnd = getList(destination.droppableId);

      listEnd.cards.splice(destination.index, 0, ...card);
    }
  }

  return (
    <Container>
      <TitleBar title="Perfil do Consumidor" />
      <div className="content">
        <DragDropContext onDragEnd={onDragEnd}>
          <FirstLine>
            <FirstColumn>
              {!gains.loading && (
                <Droppable droppableId={gains.id}>
                  {provided => (
                    <CardsContainer
                      {...provided.droppableProps}
                      ref={provided.innerRef}
                    >
                      <h4>Ganhos</h4>

                      {gains.cards.map((card, index) => (
                        <ContentCard
                          key={card.id}
                          index={index}
                          text={card.text}
                          id={card.id}
                        />
                      ))}

                      <ActionButton addCard={addGains} />
                      {provided.placeholder}
                    </CardsContainer>
                  )}
                </Droppable>
              )}
            </FirstColumn>
            <SecondColumn>
              {!pains.loading && (
                <Droppable droppableId={pains.id}>
                  {provided => (
                    <CardsContainer
                      {...provided.droppableProps}
                      ref={provided.innerRef}
                    >
                      <h4>Dores</h4>

                      {pains.cards.map((card, index) => (
                        <ContentCard
                          key={card.id}
                          index={index}
                          text={card.text}
                          id={card.id}
                        />
                      ))}

                      <ActionButton addCard={addPains} />
                      {provided.placeholder}
                    </CardsContainer>
                  )}
                </Droppable>
              )}
            </SecondColumn>
          </FirstLine>
          <SecondLine>
            {!jobs.loading && (
              <Droppable droppableId={jobs.id}>
                {provided => (
                  <CardsContainer
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                  >
                    <h4>Trabalhos</h4>

                    {jobs.cards.map((card, index) => (
                      <ContentCard
                        key={card.id}
                        index={index}
                        text={card.text}
                        id={card.id}
                      />
                    ))}

                    <ActionButton addCard={addJobs} />
                    {provided.placeholder}
                  </CardsContainer>
                )}
              </Droppable>
            )}
          </SecondLine>
        </DragDropContext>
      </div>
    </Container>
  );
}
