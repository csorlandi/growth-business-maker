import React from 'react';

import TitleBar from '~/components/TitleBar';

import { Container } from './styles';

export default function ValueMap() {
  return (
    <Container>
      <TitleBar title="Mapa de Valor" />
    </Container>
  );
}
