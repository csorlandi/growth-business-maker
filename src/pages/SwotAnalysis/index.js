import React from 'react';

import TitleBar from '~/components/TitleBar';

import { Container } from './styles';

export default function SwotAnalysis() {
  return (
    <Container>
      <TitleBar title="Análise FOFO" />
    </Container>
  );
}
