import React from 'react';

import TitleBar from '~/components/TitleBar';

import { Container } from './styles';

export default function BusinessModel() {
  return (
    <Container>
      <TitleBar title="Canvas de Modelo de Negócio" />
    </Container>
  );
}
